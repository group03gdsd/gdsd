<?php include('header.php'); ?>

<div class="contentText contextSingle">
    <p>
        <img class="thumbnail" src="assets/manases.jpg" width="200" height="200" alt="Manny" />
        <span class="name">Manas&eacute;s Jes&uacute;s Galindo Bello</span>&nbsp; &nbsp;  Team Lead<br/>
        Mexico<br/><br/>
        Education:
    </p>

    <ul>
        <li>B.Sc. Computer Engineering – Oaxaca, Mexico</li>
        <li>Master in Mobile Business – Barcelona, Spain</li>
        <li>Student of M.Sc. Global Software Development – Fulda, Germany</li>
    </ul>
</div>

<?php include('footer.php'); ?>