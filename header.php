<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="assets/style.css" />

        <title>Global Distributed Software Development – Group 03</title>
    </head>

    <body>
        <div id="page">

            <div id="header">
                <h1>Global Distributed Software Development</h1>
                <h2>Group 03 – Global Team</h2>

            </div>
            <div id="bar">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="anil.php">Anil Manzoor</a></li>
                    <li><a href="hassaan.php">Hassaan Ahmed</a></li>
                    <li><a href="intesar.php">Intesar Haider</a></li>
                    <li><a href="manases.php">Manasés Galindo</a></li>
                </ul>
            </div>