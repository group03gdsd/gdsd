<?php include('header.php'); ?>

    <div class="contentTitle"><h1>Milestone 0 Website - Developed By Hassaan Ahmed Rana</h1></div>
    <div class="contentText">
       <p>
           <img src="assets/hassaan.jpg" width="300" height="300" style="margin: 0 10px 10px 0;float:left;" />
            <em>Name: Hassaan Ahmed Rana</em> <br/>
            <em>From: Pakistan</em> <br/>
            <em>Education: Bachelors of Science in Computer Science</em> <br/>
            <em>University: University of Karachi, Karachi, Pakistan</em> <br/>
            <em>Program @ HS-Fulda: MSc. Global Software Development</em> <br/>
        </p>
    </div>

<?php include('footer.php'); ?>