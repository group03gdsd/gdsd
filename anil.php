<?php include('header.php'); ?>

    <div class="contentTitle"><h1>Milestone 0 Website - Developed By Anil Manzoor</h1></div>
    <div class="contentText">
       <p>
           <img src="assets/anil.jpg" width="344" height="344" style="margin: 0 10px 10px 0;float:left;" />
            <em>Name: Anil Manzoor</em> <br/>
            <em>From: Pakistan</em> <br/>
            <em>Education: Bachelor of Science - Computer Science</em> <br/>
            <em>University: University of Karachi, Karachi, Pakistan</em> <br/>
            <em>Program @ HS-Fulda: MSc. Global Software Development</em> <br/>
        </p>
    </div>

<?php include('footer.php'); ?>